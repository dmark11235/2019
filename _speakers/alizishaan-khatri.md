---
name: Alizishaan Khatri
talks:
- "Serverless Deep Learning with Python"
---
Alizishaan's professional passions revolve around two things : using
technology to solve real-world problems and sharing solutions with the
community. Employed as a Machine Learning Engineer with Kony Inc, he builds
scalable solutions to problems in the Natural Language Processing space.
Over a summer in the not so distant past, he designed and built an offensive
content detection system for a Silicon Valley startup. Past industry
projects include a price-prediction system for cars and a status
communication system that minimized false alerts.

Outside of work, Alizishaan's passions include mountaineering, skiing,
travelling and photography
