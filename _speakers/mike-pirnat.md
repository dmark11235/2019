---
name: Mike Pirnat
talks:
- "Dungeons \u0026 Dragons \u0026 Python: Epic Adventures with Prompt-Toolkit and Friends"
---
Mike Pirnat has been enjoying working with and sharing the joy of Python for
nearly 20 years. A founding member of ClePy and an organizer of PyOhio, he’s
also the author of _[How to Make Mistakes in
Python](http://www.oreilly.com/programming/free/how-to-make-mistakes-in-
python.csp)_ and he co-hosted and produced [From Python Import
Podcast](http://frompythonimportpodcast.com) before its long slumber. You
can find him online at [mike.pirnat.com](https://mike.pirnat.com).
