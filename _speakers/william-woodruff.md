---
name: William Woodruff
talks:
- "Improving PyPI\u0027s security with Two Factor Authentication"
---
William Woodruff is a security engineer at Trail of Bits, working between
the research and engineering practices. On the research side, he works to
improve the state of program analysis, specifically in the domains of
fuzzing and automated vulnerability reasoning. In engineering, he works with
clients to create and extend the security components of their systems.
Outside of work he enjoys cycling, hiking, and open source development.
