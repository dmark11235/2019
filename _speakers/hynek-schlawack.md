---
name: Hynek Schlawack
talks:
- "Maintaining a Python Project When It\u2019s Not Your Job"
---
Hynek Schlawack is a lead infrastructure and software engineer from Berlin,
a PSF fellow, a maintainer of too many open source projects, and a
contributor to even more.

His main areas of interest are networks, security, and robust software.
