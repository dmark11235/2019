---
duration: 40
presentation_url: null
room: PennTop South
slot: 2019-10-05 17:00:00-04:00
speakers:
- Meg Ray
title: 'Because Equity: Python FTW'
type: keynote
video_url: null
---

How to teach Python, how Python fits into the broader CS for All movement in
education, and what you can do to make a difference no matter what your
experience level.
